use std::net::IpAddr;

use serde::{Deserialize, Serialize};
use sqlx::types::{Decimal, chrono::NaiveDateTime};

#[derive(Deserialize)]
pub struct Register {
    pub email: String,
    pub pass_hash: String,
    pub nickname: String,
}

#[derive(Deserialize)]
pub struct Login {
    pub email: String,
    pub pass_hash: String,
}

#[derive(Deserialize)]
pub struct TopUp {
    pub user_id: i64,
    pub money: Decimal,
}

#[derive(Deserialize)]
pub struct ChangePass {
    pub user_id: i64,
    pub original_pass_hash: String,
    pub new_pass_hash: String,
}

#[derive(Deserialize)]
pub struct ChangeNickname {
    pub user_id: i64,
    pub new_nickname: String,
}

#[derive(Deserialize, Serialize)]
pub struct User {
    pub user_id: i64,
    pub nickname: String,
    pub cash: Decimal,
}

#[derive(Deserialize)]
pub struct List {
    pub user_id: i64,
}

#[derive(Deserialize)]
pub struct Info {
    pub user_id: i64,
}

#[derive(Deserialize)]
pub struct GetStatus {
    pub intra_ip: IpAddr,
}

#[derive(Debug, Serialize)]
pub struct Instance {
    pub intra_ip: IpAddr,
    pub inter_ip: IpAddr,
    pub vcpu: i16,
    pub vram: i16,
    pub vssd: i16,
    pub inst_name: String,
}

#[derive(Debug, Deserialize)]
pub struct Toggle {
    pub intra_ip: IpAddr,
    pub up_state: bool,
}

#[derive(Debug, Serialize)]
pub struct Status {
    pub last_up: NaiveDateTime,
    pub up_state: bool,
    pub vcpu_usage: Decimal,
    pub vram_usage: Decimal,
    pub net_traffic: Decimal,
}

#[derive(Debug, Serialize)]
pub struct Cluster {
    pub region: String,
}

#[derive(Debug, Deserialize)]
pub struct Buy {
    pub user_id: i64,
    pub curr_time: NaiveDateTime,
    pub intra_ip: IpAddr,
    pub inter_ip: IpAddr,
    pub vcpu: i16,
    pub vram: i16,
    pub vssd: i16,
    pub price: Decimal,
}

#[derive(Debug, Deserialize)]
pub struct Remove {
    pub intra_ip: IpAddr,
}
