use axum::{extract::State, http::StatusCode, Json};
use futures::TryStreamExt;
use sqlx::{PgPool, Row};

use crate::models::Cluster;

#[axum_macros::debug_handler]
pub async fn get(State(pool): State<PgPool>) -> Result<Json<Vec<Cluster>>, (StatusCode, String)> {
    let mut rows = sqlx::query(
        "SELECT *
        FROM server_cluster",
    )
    .fetch(&pool);

    let mut clusters: Vec<Cluster> = vec![];
    while let Some(row) = rows.try_next().await.unwrap() {
        let region: String = row.try_get("region").unwrap();

        clusters.push(Cluster { region });
    }

    Ok(Json(clusters))
}
