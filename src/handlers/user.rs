use axum::{extract::State, http::StatusCode, Json};
use sqlx::{types::Decimal, PgPool, Row};

use crate::models::{Login, User, Info, TopUp, ChangePass, ChangeNickname};

#[axum_macros::debug_handler]
pub async fn login(
    State(pool): State<PgPool>,
    Json(payload): Json<Login>,
) -> Result<Json<User>, (StatusCode, String)> {
    println!("User login: {}", payload.email);

    let row = sqlx::query(
        "SELECT user_id, nickname, email, pass_hash, cash
        FROM cloud_user 
        WHERE email = $1;",
    )
    .bind(payload.email)
    .fetch_optional(&pool)
    .await
    .unwrap();

    match row {
        Some(r) => {
            let pass_hash: &str = r.try_get("pass_hash").unwrap();
            if payload.pass_hash == pass_hash {
                let nickname: &str = r.try_get("nickname").unwrap();
                let user_id: i64 = r.try_get("user_id").unwrap();
                let cash: Decimal = r.try_get("cash").unwrap();

                Ok(Json(User {
                    user_id,
                    nickname: nickname.to_string(),
                    cash,
                }))
            } else {
                Err((StatusCode::UNAUTHORIZED, "密码错误".to_string()))
            }
        }
        None => Err((StatusCode::NOT_FOUND, "用户不存在".to_string())),
    }
}

#[axum_macros::debug_handler]
pub async fn info(
    State(pool): State<PgPool>,
    Json(payload): Json<Info>,
) -> Result<Json<User>, (StatusCode, String)> {
    let row = sqlx::query(
        "SELECT user_id, nickname, cash
        FROM cloud_user 
        WHERE user_id = $1;",
    )
    .bind(payload.user_id)
    .fetch_optional(&pool)
    .await
    .unwrap();

    match row {
        Some(r) => {
            let nickname: &str = r.try_get("nickname").unwrap();
            let cash: Decimal = r.try_get("cash").unwrap();

            Ok(Json(User {
                user_id: payload.user_id,
                nickname: nickname.to_string(),
                cash,
            }))
        }
        None => Err((StatusCode::NOT_FOUND, "用户不存在".to_string())),
    }
}

#[axum_macros::debug_handler]
pub async fn top_up(
    State(pool): State<PgPool>,
    Json(payload): Json<TopUp>,
) -> StatusCode {
    let result = sqlx::query(
        "UPDATE cloud_user
        SET cash = cash + $1
        WHERE user_id = $2;",
    )
    .bind(payload.money)
    .bind(payload.user_id)
    .execute(&pool)
    .await
    .unwrap();

    if result.rows_affected() != 0 {
        StatusCode::OK
    } else {
        StatusCode::NOT_FOUND
    }
}

#[axum_macros::debug_handler]
pub async fn change_pass(
    State(pool): State<PgPool>,
    Json(payload): Json<ChangePass>,
) -> (StatusCode, String) {
    let row = sqlx::query(
        "SELECT user_id, pass_hash
        FROM cloud_user
        WHERE user_id = $1"
    )
    .bind(payload.user_id)
    .fetch_optional(&pool)
    .await
    .unwrap();

    match row {
        Some(r) => {
            let pass_hash: &str = r.try_get("pass_hash").unwrap();
            if pass_hash != payload.original_pass_hash {
                return (StatusCode::UNAUTHORIZED, "原密码错误".to_string())
            }
            
            let result = sqlx::query(
                "UPDATE cloud_user
                SET pass_hash = $1
                WHERE user_id = $2;",
            )
            .bind(payload.new_pass_hash)
            .bind(payload.user_id)
            .execute(&pool)
            .await
            .unwrap();

            if result.rows_affected() != 0 {
                (StatusCode::OK, "密码修改成功".to_string())
            } else {
                (StatusCode::INTERNAL_SERVER_ERROR, "密码修改失败".to_string())
            }
        },
        None => (StatusCode::NOT_FOUND, "用户不存在".to_string())
    }
}

#[axum_macros::debug_handler]
pub async fn change_nickname(
    State(pool): State<PgPool>,
    Json(payload): Json<ChangeNickname>,
) -> StatusCode {
    let result = sqlx::query(
        "UPDATE cloud_user
        SET nickname = $1
        WHERE user_id = $2;",
    )
    .bind(payload.new_nickname)
    .bind(payload.user_id)
    .execute(&pool)
    .await
    .unwrap();

    if result.rows_affected() != 0 {
        StatusCode::OK
    } else {
        StatusCode::INTERNAL_SERVER_ERROR
    }
}
