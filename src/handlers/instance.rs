use std::net::IpAddr;

use axum::{extract::State, http::StatusCode, Json};
use chrono::NaiveDateTime;
use futures::TryStreamExt;
use sqlx::{PgPool, Row, types::Decimal};

use crate::models::{Instance, Status, Toggle, Buy, List, GetStatus, Remove};

#[axum_macros::debug_handler]
pub async fn list(
    State(pool): State<PgPool>,
    Json(payload): Json<List>,
) -> Result<Json<Vec<Instance>>, (StatusCode, String)> {
    let mut rows = sqlx::query(
        "SELECT i.intra_ip, inter_ip, vcpu, vram, vssd, ip, inst_name
        FROM machine_instance i
        JOIN subscribe s ON i.intra_ip = s.intra_ip
        JOIN cloud_user u ON s.user_id = u.user_id
        WHERE u.user_id = $1;",
    )
    .bind(payload.user_id)
    .fetch(&pool);

    let mut instances: Vec<Instance> = vec![];
    while let Some(row) = rows.try_next().await.unwrap() {
        let intra_ip: IpAddr = row.try_get("intra_ip").unwrap();
        let inter_ip: IpAddr = row.try_get("inter_ip").unwrap();
        let vcpu: i16 = row.try_get("vcpu").unwrap();
        let vram: i16 = row.try_get("vram").unwrap();
        let vssd: i16 = row.try_get("vssd").unwrap();
        let inst_name: &str = row.try_get("inst_name").unwrap();

        instances.push(Instance {
            intra_ip,
            inter_ip,
            vcpu,
            vram,
            vssd,
            inst_name: inst_name.to_owned(),
        });
    }

    Ok(Json(instances))
}

#[axum_macros::debug_handler]
pub async fn status(
    State(pool): State<PgPool>,
    Json(payload): Json<GetStatus>,
) -> Result<Json<Status>, (StatusCode, String)> {
    let row = sqlx::query(
        "SELECT last_up, up_state, vcpu_usage, vram_usage, net_traffic
        FROM machine_state s
        JOIN machine_instance i ON i.intra_ip = s.intra_ip
        WHERE i.intra_ip = $1;",
    )
    .bind(payload.intra_ip)
    .fetch_optional(&pool)
    .await
    .unwrap();
    
    match row {
        Some(r) => {
            let last_up: NaiveDateTime = r.try_get("last_up").unwrap();
            let up_state: bool = r.try_get("up_state").unwrap();
            let vcpu_usage: Decimal = r.try_get("vcpu_usage").unwrap();
            let vram_usage: Decimal = r.try_get("vram_usage").unwrap();
            let net_traffic: Decimal = r.try_get("net_traffic").unwrap();

            Ok(Json(Status {
                last_up,
                up_state,
                vcpu_usage,
                vram_usage,
                net_traffic,
            }))
        },
        None => Err((StatusCode::NOT_FOUND, "实例不存在".to_string()))
    }
}

#[axum_macros::debug_handler]
pub async fn toggle(
    State(pool): State<PgPool>,
    Json(payload): Json<Toggle>,
) -> StatusCode {
    let result = sqlx::query(
        "UPDATE machine_state
        SET up_state = $1
        WHERE intra_ip = $2",
    )
    .bind(payload.up_state)
    .bind(payload.intra_ip)
    .execute(&pool)
    .await
    .unwrap();
    
    if result.rows_affected() == 1 {
        StatusCode::OK
    } else {
        StatusCode::NOT_FOUND
    }
}

#[axum_macros::debug_handler]
pub async fn buy(
    State(pool): State<PgPool>,
    Json(payload): Json<Buy>,
) -> StatusCode {
    let result = sqlx::query(
        "INSERT INTO machine_instance
        VALUES ($1, $2, $3, $4, $5, $6, '10.0.1.1');
        INSERT INTO machine_state(last_up, intra_ip)
        VALUES ($7, $1)
        INSERT INTO subscribe
        VALUES ($8, $1, $7)
        "
    )
    .bind(payload.intra_ip)
    .bind(payload.inter_ip)
    .bind(payload.vcpu)
    .bind(payload.vram)
    .bind(payload.vssd)
    .bind(payload.price)
    .bind(payload.curr_time)
    .bind(payload.user_id)
    .execute(&pool)
    .await
    .unwrap();

    if result.rows_affected() != 0 {
        StatusCode::OK
    } else {
        StatusCode::INTERNAL_SERVER_ERROR
    }
}

#[axum_macros::debug_handler]
pub async fn remove(
    State(pool): State<PgPool>,
    Json(payload): Json<Remove>,
) -> StatusCode {
    let result = sqlx::query(
        "DELETE FROM subscribe
        WHERE intra_ip = $1;
        DELETE FROM machine_state
        WHERE intra_ip = $1;
        DELETE FROM machine_instance
        WHERE intra_ip = $1;"
    )
    .bind(payload.intra_ip)
    .execute(&pool)
    .await
    .unwrap();

    if result.rows_affected() != 0 {
        StatusCode::OK
    } else {
        StatusCode::NOT_FOUND
    }
}
