mod handlers;
mod models;

use axum::{
    routing::{get, post},
    Router,
};
use sqlx::postgres::PgPoolOptions;
use std::net::SocketAddr;

#[tokio::main]
async fn main() {
    tracing_subscriber::fmt::init();

    let pool = PgPoolOptions::new()
        .max_connections(5)
        .connect("postgres://postgres:ahucloud@localhost:5432")
        .await
        .expect("Cannot connect to database");

    let app = Router::new()
        .route("/",                     get (|| async { "Hello, AhuCloud!" }))
        .route("/login",                post(handlers::user::login))
        .route("/user/info",            post(handlers::user::info))
        .route("/user/top-up",          post(handlers::user::top_up))
        .route("/user/change-pass",     post(handlers::user::change_pass))
        .route("/user/change-nickname", post(handlers::user::change_nickname))
        .route("/instance/buy",         post(handlers::instance::buy))
        .route("/instance/status",      post(handlers::instance::status))
        .route("/instance/list",        post(handlers::instance::list))
        .route("/instance/toggle",      post(handlers::instance::toggle))
        .route("/instance/remove",      post(handlers::instance::remove))
        .route("/cluster/get",          get (handlers::cluster::get))
        .with_state(pool);

    let addr = SocketAddr::from(([0, 0, 0, 0], 3000));
    tracing::debug!("listening on: {}", addr);

    axum::Server::bind(&addr)
        .serve(app.into_make_service())
        .await
        .unwrap();
}
